package com.xaicode.learn.ads.linkedlist;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * unsorted single linked list
 *
 * @author Locker xaicode@sina.com
 */
public class UnsortedSingleLinkedList {
    public static void main(String[] args) {
        SLL sll = new SLL();
        Node node1 = new Node("1", "k1", "v1");
        Node node2 = new Node("2", "k2", "v2");
        Node node3 = new Node("3", "k3", "v3");
        Node node4 = new Node("4", "k4", "v4");
        sll.add(node1);
        sll.add(node2);
        sll.add(node3);
        sll.add(node4);
        sll.list();
    }

    /**
     * single linked list
     */
    static class SLL {
        /**
         * all nodes data
         */
        private Node nodes = new Node("0", "", "");

        public void add(Node node) {
            Node ite = nodes;
            // find the last node
            while (ite.next != null) {
                ite = ite.next;
            }
            ite.next = node;
        }

        public void list() {
            // skip the first null node
            Node temp = nodes.next;
            while (temp != null) {
                System.out.println(temp);
                temp = temp.next;
            }

        /*
            Node(no=1, key=k1, val=v1)
            Node(no=2, key=k2, val=v2)
            Node(no=3, key=k3, val=v3)
            Node(no=4, key=k4, val=v4)
            end
         */
            System.out.println("end");
        }
    }

    /**
     * data node for linked list
     */
    @Setter
    @Getter
    @ToString
    static class Node {
        /**
         * current node number
         */
        String no;

        String key;

        Object val;

        /**
         * next node.
         * It will be the last node when this <code>next</code> equals <code>null</code>
         */
        @ToString.Exclude
        Node next;

        public Node(String no, String key, Object val) {
            this.no = no;
            this.key = key;
            this.val = val;
        }
    }

}